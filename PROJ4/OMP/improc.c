#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <omp.h>
#include "improc.h"
#include "image_template.h"

int main(int argc,char *argv[]){
    float **kernel,**gkernel,**vkernel,**vgkernel,**rawPtr;
    float *raw;
    char fileNameHImage[100] = "horizontal_gradient.pgm";
    char fileNameVImage[100] = "vertical_gradient.pgm";
    char fileNameMImage[100] = "magnitude.pgm";
    char fileNamePImage[100] = "phase.pgm";
    rawPtr = &raw;

    int imWidth,imHeight,kernelSize,gkernelSize;
    char *fn;
    float sigma;
    int numThreads = 2;

    if (argc < 4){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value> <Number_Threads>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
        numThreads = atoi(argv[3]);
    }
    omp_set_num_threads(numThreads);

    read_image_template<float>(fn,rawPtr,&imWidth,&imHeight);
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *magnitude = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *phase = (float*)malloc(sizeof(float)*imWidth*imHeight);
    int offset = (int)((imHeight*imWidth)/numThreads);
    int chunkWidth = imWidth;
    int chunkHeight = (int)(imHeight/numThreads);

    kernel = gaussian(sigma,&kernelSize);
    gkernel = gaussianDeriv(sigma,&gkernelSize);
    vkernel = transpose(kernel,kernelSize,1);
    vgkernel = transpose(gkernel,kernelSize,1);

    struct timeval start,end;
    gettimeofday(&start,NULL);
    int i = 0;
    #pragma omp parallel
    {
        #pragma omp for private (i)
        for(i=0;i<numThreads;i++){
            convolve<float>(vkernel, raw + i*offset, partialSmoothImageX + i*offset, imWidth, imHeight, chunkWidth, chunkHeight, 1, kernelSize, i*offset);
            convolve<float>(kernel, raw + i*offset, partialSmoothImageY + i*offset, imWidth, imHeight, chunkWidth, chunkHeight, kernelSize, 1, i*offset);
        }
        #pragma omp for private (i)
        for(i=0;i<numThreads;i++){
            convolve<float>(gkernel, partialSmoothImageX + i*offset, Gx + i*offset, imWidth, imHeight, chunkWidth, chunkHeight, kernelSize, 1, i*offset);
            convolve<float>(vgkernel, partialSmoothImageY + i*offset, Gy + i*offset, imWidth, imHeight, chunkWidth, chunkHeight, 1, kernelSize, i*offset);
        }
        #pragma omp for private (i)
        for(i=0;i<numThreads;i++){
            magnitudePhase<float,float>(Gx + i*offset, Gy + i*offset , magnitude + i*offset ,phase + i*offset , chunkWidth , chunkHeight);
        }
    }

    gettimeofday(&end,NULL);
    printf("OMP Time: %lfs\n",((double)(end.tv_sec*1000000 + end.tv_usec)/1000000 - (double)(start.tv_sec*1000000 + start.tv_usec)/1000000));

    write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);
    write_image_template<float>(fileNameMImage,magnitude,imWidth,imHeight);
    write_image_template<float>(fileNamePImage,phase,imWidth,imHeight);

    free2D<float>(kernel,kernelSize,1);
    free2D<float>(gkernel,gkernelSize,1);
    free2D<float>(vkernel,kernelSize,1);
    free2D<float>(vgkernel,kernelSize,1);
    free(partialSmoothImageX);
    free(partialSmoothImageY);
    free(Gx);
    free(Gy);
    free(magnitude);
    free(phase);
    free(raw);

    return 0;
}

template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight,int chunkWidth, int chunkHeight, int kernelWidth, int kernelHeight, int offset){
    int rowCenter,colCenter,yBound,yCoord,xCoord;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);

    int i,j,y,x;
    float sop = 0;
    if(kernelWidth >= kernelHeight){
        for(j=0;j<chunkHeight;j++){
            for(i=0;i<chunkWidth;i++){
                sop = 0;
                for(y=0;y<kernelHeight;y++){
                    yBound = j - (rowCenter - y)  + (int)offset/(imWidth);
                    yCoord =  j - (rowCenter - y);
                    for(x=0;x<kernelWidth;x++){
                        //check boundaries
                        xCoord = i - (colCenter - x);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yBound < 0 || yBound > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
    else{
        for(j=0;j<chunkHeight;j++){
            for(i=0;i<chunkWidth;i++){
                sop = 0;
                for(x=0;x<kernelWidth;x++){
                    xCoord = i - (colCenter - x);
                    for(y=0;y<kernelHeight;y++){
                        //check boundaries
                        yBound = j - (rowCenter - y)  + (int)offset/(imWidth);
                        yCoord =  j - (rowCenter - y);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yBound < 0 || yBound > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
}

template <class T,class U>
void magnitudePhase(T *horizontal, T *vertical, U *magnitude, U *phase, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            magnitude[i*width + j] = sqrt(pow(vertical[i*width + j],2) + pow(horizontal[i*width + j],2));
            phase[i*width + j] = atan2(vertical[i*width + j],horizontal[i*width + j]);
        }
    }
}

void rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float matCopy[height][width];
    center = height/2;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            matCopy[i][j] = mat[i][j];
        }
    }
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                mat[i][j] = matCopy[j][center + offset];
            }
        }
    }
}

void rotate1801D(float **mat, int width, int height){
    float temp;
    int center,i,offset;
    if(width < height){
        center = height/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[i][0];
            mat[i][0] = mat[center + offset][0];
            mat[center+offset][0] = temp;
        }
    }
    else{
        center = width/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[0][i];
            mat[0][i] = mat[0][center + offset];
            mat[0][center+offset] = temp;
        }
    }
}


float **gaussian(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **gaussianDeriv(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        kernel[0][i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **transpose(float **mat, int width, int height){
    float **newMat;
    int i,j;
    if (width == height){
        newMat = create2D(width,height);
        for(i=0;i<height;i++){
            for(j=0;j<width;j++)newMat[i][j] = mat[j][i];
        }
    }
    else{
        newMat = create2D(height,width);
        if (width > height)for(j=0;j<width;j++)newMat[j][0] = mat[0][j];
        else for(j=0;j<height;j++)newMat[0][j] = mat[j][0];
    }
    return newMat;
}

float **create2D(int width, int height){
    int i;
    float **mat;
    mat = (float**)malloc(sizeof(float*)*height);
    for(i=0;i<height;i++)mat[i] = (float*)malloc(sizeof(float)*width);
    return mat;
}
template <class T>
void free2D(T **mat, int width, int height){
    int i;
    for(i=0;i<height;i++)free(mat[i]);
}

void printMat(float **mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i][j]);
        }
        printf("\n");
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

struct threadConvolveData{
    int threadId;
    float **kernel;
    float *inputIm;
    float *outputIm;
    int width;
    int height;
    int chunkWidth;
    int chunkHeight;
    int kernelWidth;
    int kernelHeight;
    int offset;
};
struct threadMagnitudePhaseData{
    int threadId;
    float *horizontal;
    float *vertical;
    float *magnitude;
    float *phase;
    int width;
    int height;
};
template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int chunkWidth, int chunkHeight, int kernelWidth, int kernelHeight,int offset);
template <class T, class U>
void magnitudePhase(T *horizontal, T *vertical, U *magnitude, U *phase, int width, int height);
float **rotate902D(float **mat, int width, int height);
float **rotate1801D(float **mat, int width, int height);
float **gaussian(float sigma, int *size);
float **gaussianDeriv(float sigma, int *size);
float **transpose(float **mat, int width, int height);
float **create2D(int width, int height);
template <class T>
void free2D(T **mat, int width, int height);
template <class T,class U>
void copy1D2D(T *mat1,U **mat2,int width,int height,int offset);
template <class T,class U>
void copy2D1D(T **mat1,U *mat2,int width,int height,int offset);
void printMat(float **mat, int width, int height);
void printMat1D(float *mat, int width, int height);
void *threadConvolve(void *threadid);
void *threadMagnitudePhase(void *threadid);

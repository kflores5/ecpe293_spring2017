template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight);
template <class T, class U>
void magnitudePhase(T *vertical, T *horizontal, U *magnitude, U *gradient, int width, int height);
template <class T>
void nonMaxsuppression(T *mag, T *phase, T *outImg, int width, int height);
template <class T>
float findPeak(T *mag, T *phase, int i, int j, int width, int height);
template <class T>
void hysteresis(T *nonMaxImg, T *outImg, int width, int height);
int comp( const void *a, const void *b );
void rotate902D(float **mat, int width, int height);
void rotate1801D(float **mat, int width, int height);
float **gaussian(float sigma, int *size);
float **gaussianDeriv(float sigma, int *size);
float **transpose(float **mat, int width, int height);
float **create2D(int width, int height);
template <class T>
void free2D(T **mat, int width, int height);
void printMat(float **mat, int width, int height);
void printMat1D(float *mat, int width, int height);

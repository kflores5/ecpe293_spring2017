#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <mpi.h>
#include "improc.h"
#include "image_template.h"

#define PI 3.14159265

int main(int argc,char *argv[]){

    //Initialize MPI
    int rc;
    char *fn;

    rc = MPI_Init(&argc,&argv);
    if(rc != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD,rc);

    int commSize,commRank;
    MPI_Comm_size(MPI_COMM_WORLD,&commSize);
    MPI_Comm_rank(MPI_COMM_WORLD,&commRank);
    int imWidth,imHeight;
    float *raw;
    float sigma;
    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
    }

    float *Gx;
    float *Gy;
    float *mag;
    float *phase;
    float *nonMaxImg;
    float *edgeImg;
    if(commRank==0){
        read_image_template<float>(fn,&raw,&imWidth,&imHeight);
        Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
        Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
        mag = (float*)malloc(sizeof(float)*imWidth*imHeight);
        phase = (float*)malloc(sizeof(float)*imWidth*imHeight);
        nonMaxImg = (float*)malloc(sizeof(float)*imWidth*imHeight);
        edgeImg = (float*)malloc(sizeof(float)*imWidth*imHeight);
    }
    MPI_Bcast(&imWidth,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&imHeight,1,MPI_INT,0,MPI_COMM_WORLD);
    char fileNameHImage[30] = "horizontal_gradient.pgm";
    char fileNameVImage[30] = "vertical_gradient.pgm";
    char fileNameMImage[10] = "mag.pgm";
    char fileNamePImage[15] = "phase.pgm";
    char fileNameNImage[20] = "nonMaxImage.pgm";
    char fileNameEImage[10] = "edge.pgm";

    float **kernel,**gkernel,**vkernel,**vgkernel;
    int kernelSize,gkernelSize,chunkWidth,chunkHeight;
    // MPI Variables
    MPI_Status status;

    kernel = gaussian(sigma,&kernelSize);
    gkernel = gaussianDeriv(sigma,&gkernelSize);
    vkernel = transpose(kernel,kernelSize,1);
    vgkernel = transpose(gkernel,kernelSize,1);

    chunkWidth = imWidth;
    chunkHeight = imHeight/commSize;
    int ghostRowSize = int(kernelSize/2) + 1;

    float *chunkGx = (float*)malloc(sizeof(float)*chunkWidth*chunkHeight);
    float *chunkGy = (float*)malloc(sizeof(float)*chunkWidth*chunkHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+(ghostRowSize*2)));
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+(ghostRowSize*2)));
    float *chunkImg = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+(ghostRowSize*2)));
    float *chunkMag = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+2));
    float *chunkPhase = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+2));
    float *chunkNonMaxImg = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+2));
    float *chunkEdgeImg = (float*)malloc(sizeof(float)*chunkWidth*(chunkHeight+2));

    //Start timer
    struct timeval start,end;
    if(commRank==0){
        gettimeofday(&start,NULL);
    }

    //Splitting image to chunks
    MPI_Scatter(raw,chunkWidth*chunkHeight,MPI_FLOAT,chunkImg+chunkWidth*ghostRowSize,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);

    //Adding ghost rows from original image
    addGhostRowsAll<float>(chunkImg,commSize,commRank,ghostRowSize,chunkWidth,chunkHeight);

    //Convolution
    int offset = imWidth*ghostRowSize;
    convolve(vkernel,chunkImg+offset,partialSmoothImageX+offset,chunkWidth,chunkHeight,1,kernelSize);
    convolve(gkernel,partialSmoothImageX+offset,chunkGx,chunkWidth,chunkHeight,kernelSize,1);
    convolve(kernel,chunkImg,partialSmoothImageY,chunkWidth,chunkHeight+ghostRowSize*2,kernelSize,1);
    convolve(vgkernel,partialSmoothImageY+offset,chunkGy,chunkWidth,chunkHeight,1,kernelSize);
    //Magnitude and Phase
    magnitudePhase<float,float>(chunkGx,chunkGy,chunkMag+chunkWidth,chunkPhase+chunkWidth,chunkWidth,chunkHeight);
    //Add ghost rows from magnitude and phase
    addGhostRowsAll<float>(chunkMag,commSize,commRank,1,chunkWidth,chunkHeight);
    addGhostRowsAll<float>(chunkPhase,commSize,commRank,1,chunkWidth,chunkHeight);
    //Nonmax Suppression
    nonMaxsuppression<float>(chunkMag,chunkPhase,chunkNonMaxImg,chunkWidth,chunkHeight);

    MPI_Gather(chunkGx,chunkWidth*chunkHeight,MPI_FLOAT,Gx,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);
    MPI_Gather(chunkGy,chunkWidth*chunkHeight,MPI_FLOAT,Gy,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);
    MPI_Gather(chunkMag+chunkWidth,chunkWidth*chunkHeight,MPI_FLOAT,mag,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);
    MPI_Gather(chunkPhase+chunkWidth,chunkWidth*chunkHeight,MPI_FLOAT,phase,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);
    MPI_Gather(chunkNonMaxImg+chunkWidth,chunkWidth*chunkHeight,MPI_FLOAT,nonMaxImg,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);
    //Calculate high threshold intensity
    float tHi;
    if (commRank == 0)
        tHi = getHighThresh<float>(nonMaxImg,imWidth,imHeight);
    MPI_Bcast(&tHi,1,MPI_FLOAT,0,MPI_COMM_WORLD);
    //Hysteresis and edge linking
    hysteresis<float>(chunkNonMaxImg,chunkEdgeImg,tHi,commSize,commRank,chunkWidth,chunkHeight);
    MPI_Gather(chunkEdgeImg+chunkWidth,chunkWidth*chunkHeight,MPI_FLOAT,edgeImg,chunkWidth*chunkHeight,MPI_FLOAT,0,MPI_COMM_WORLD);

    if(commRank==0){
        gettimeofday(&end,NULL);
        printf("MPI Time: %lfs\n",((double)(end.tv_sec*1000000 + end.tv_usec)/1000000 - (double)(start.tv_sec*1000000 + start.tv_usec)/1000000));
    }

    if(commRank==0){
        write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
        write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);
        write_image_template<float>(fileNameMImage,mag,imWidth,imHeight);
        write_image_template<float>(fileNamePImage,phase,imWidth,imHeight);
        write_image_template<float>(fileNameNImage,nonMaxImg,imWidth,imHeight);
        write_image_template<float>(fileNameEImage,edgeImg,imWidth,imHeight);
        free(partialSmoothImageX);
        free(partialSmoothImageY);
        free(Gx);
        free(Gy);
        free(mag);
        free(phase);
        free(raw);
        free(nonMaxImg);
        free(edgeImg);
    }

    free2D<float>(kernel,kernelSize,1);
    free2D<float>(gkernel,gkernelSize,1);
    free2D<float>(vkernel,kernelSize,1);
    free2D<float>(vgkernel,kernelSize,1);
    free(chunkImg);
    free(chunkGx);
    free(chunkGy);
    free(chunkMag);
    free(chunkPhase);
    free(chunkNonMaxImg);
    free(chunkEdgeImg);
    MPI_Finalize();

    return 0;
}
template <class T>
void addGhostRowsAll(T *chunkImg,int commSize,int commRank,int ghostRowSize,int chunkWidth,int chunkHeight){
    MPI_Status status;
    for(int i=0;i<commSize-1;i++){
        if(commRank==i)
            MPI_Send(chunkImg+chunkWidth*chunkHeight,chunkWidth*ghostRowSize,MPI_FLOAT,i+1,100,MPI_COMM_WORLD);
    }
    for(int i=1;i<commSize;i++){
        if(commRank==i)
            MPI_Recv(chunkImg,chunkWidth*ghostRowSize,MPI_FLOAT,i-1,100,MPI_COMM_WORLD,&status);
    }
    for(int i=1;i<commSize;i++){
        if(commRank==i)
            MPI_Send(chunkImg+chunkWidth*ghostRowSize,chunkWidth*ghostRowSize,MPI_FLOAT,i-1,101,MPI_COMM_WORLD);
    }
    for(int i=0;i<commSize-1;i++){
        if(commRank==i)
            MPI_Recv(chunkImg+chunkWidth*(chunkHeight+ghostRowSize),chunkWidth*ghostRowSize,MPI_FLOAT,i+1,101,MPI_COMM_WORLD,&status);
    }
}

template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight){
    int rowCenter,colCenter,yCoord,xCoord;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);
    if(kernelWidth == kernelHeight)rotate902D(kernel,kernelWidth,kernelHeight);
    else rotate1801D(kernel,kernelWidth,kernelHeight);

    int i,j,y,x;
    float sop = 0;

    if(kernelWidth >= kernelHeight){
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(y=0;y<kernelHeight;y++){
                    yCoord =  j - (rowCenter - y);
                    for(x=0;x<kernelWidth;x++){
                        //check boundaries
                        xCoord = i - (colCenter - x);
                        if( xCoord < 0 || xCoord > imWidth - 1 ) continue;
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
    else{
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(x=0;x<kernelWidth;x++){
                    xCoord = i - (colCenter - x);
                    for(y=0;y<kernelHeight;y++){
                        //check boundaries
                        yCoord =  j - (rowCenter - y);
                        if( xCoord < 0 || xCoord > imWidth - 1)continue;
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
}

template <class T,class U>
void magnitudePhase(T *horizontal, T *vertical, U *mag, U *phase, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mag[i*width + j] = sqrt(pow(vertical[i*width + j],2) + pow(horizontal[i*width + j],2));
            phase[i*width + j] = atan2(horizontal[i*width + j],vertical[i*width + j]);
        }
    }
}
template <class T>
void nonMaxsuppression(T *mag, T *phase, T *outImg, int width, int height){
    int i,j,k;
    float angle,startAngle,endAngle;

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            angle = phase[i*width + j];
            angle = angle*180/PI;
            if (angle < 0){
                angle = angle + 180;
            }
            if (angle <= 22.5 || angle > 157.5){
                phase[i*width + j] = 0;
            }
            else{
                startAngle = 22.5;
                for(k=0;k<3;k++){
                    endAngle = startAngle + 45;
                    if (angle > startAngle && angle <= endAngle){
                        phase[i*width + j] = startAngle + 22.5;
                        break;
                    }
                    startAngle = endAngle;
                }
            }
        }
    }

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            outImg[i*width + j] = findPeak<T>(mag,phase,i,j,width,height);
        }
    }
}

template <class T>
float findPeak(T *mag, T *phase, int i, int j, int width, int height){
    float angle,peak;
    int xOffset, yOffset;

    angle = phase[i*width + j];
    if (angle == 0){
        xOffset = 0;
        yOffset = -1;
    }
    else if(angle == 45){
        xOffset = -1;
        yOffset = -1;
    }
    else if(angle == 90){
        xOffset = 1;
        yOffset = 0;
    }
    else if(angle == 135){
        xOffset = 1;
        yOffset = -1;
    }
    peak = mag[i*width + j];
    if( i+1 < height && i-1 >= 0 && j+1 < width && j-1 >= 0 ){
        if( ( mag[i*width + j] < mag[(i+yOffset)*width + j + xOffset] && \
            phase[i*width + j] == phase[(i+yOffset)*width + j + xOffset] ) || \
            ( mag[i*width + j] < mag[(i-yOffset)*width + j - xOffset] && \
            phase[i*width + j] == phase[(i-yOffset)*width + j - xOffset] ) ) peak = 0;
    }
    else{
        if( i-yOffset < 0 || i-yOffset >= height || j-xOffset < 0 || j-xOffset >= width ){
            if( mag[i*width + j] < mag[(i+yOffset)*width + j + xOffset] && \
                phase[i*width + j] == phase[(i+yOffset)*width + j + xOffset] ) peak = 0;
        }
        if( i+yOffset < 0 || i+yOffset >= height || j+xOffset < 0 || j+xOffset >= width ){
            if( mag[i*width + j] < mag[(i-yOffset)*width + j - xOffset] && \
                phase[i*width + j] == phase[(i-yOffset)*width + j - xOffset] ) peak = 0;
        }
    }
    return peak;
}

template <class T>
float getHighThresh(T *nonMaxImg,int width,int height){
    int i,j;
    float *sortedIntensity = (float*)malloc(sizeof(float)*width*height);
    //Copy non maximal suppression image
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            sortedIntensity[i*width + j] = nonMaxImg[i*width + j];
        }
    }
    qsort(sortedIntensity,width*height,sizeof(*sortedIntensity),comp);
    return sortedIntensity[int(width*height*.9)];
    free(sortedIntensity);
}

template <class T>
void hysteresis(T *nonMaxImg, T *outImg, float tHi, int commSize, int commRank, int width, int height){
    int i,j,x,y,terminate;
    int center = 1;
    float tLo;
    float *hImage = (float*)malloc(sizeof(float)*width*height);

    tLo = tHi/5;
    //printf("%f %f\n",tHi,tLo);

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if (!(i==0||i==height-1||j==0||j==width-1)){
                if(nonMaxImg[i*width + j] >= tHi)hImage[i*width + j] = 255;
                else if(nonMaxImg[i*width + j] < tHi && nonMaxImg[i*width + j] > tLo)hImage[i*width + j] = 125;
            }
            else hImage[i*width + j] = 125;
            outImg[i*width + j] = hImage[i*width + j];
        }
    }

    addGhostRowsAll<float>(hImage,commSize,commRank,1,width,height);

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if (hImage[i*width + j] == 125){
                for(y=0;y<2;y++){
                    terminate = 0;
                    for(x=0;x<2;x++){
                        if(!(y==center&&x==center)){
                            if( j-(center-x) < 0 || j-(center-x) >= width || \
                                i-(center-y) < 0 || i-(center-y) >= height ) continue;
                            else{
                                if (hImage[(i-y)*width + j - x] == 255){
                                    outImg[i*width+j] = 255;
                                    terminate = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (terminate == 0) outImg[i*width+j] = 0;
            }
        }
    }

    free(hImage);
}

int comp( const void *a, const void *b ){
    float *x = (float *)a;
    float *y = (float *)b;
    if( *x < *y ) return -1;
    else if( *x > *y ) return 1;
    return 0;

}

void rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float matCopy[height][width];
    center = height/2;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            matCopy[i][j] = mat[i][j];
        }
    }
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                mat[i][j] = matCopy[j][center + offset];
            }
        }
    }
}

void rotate1801D(float **mat, int width, int height){
    float temp;
    int center,i,offset;
    if(width < height){
        center = height/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[i][0];
            mat[i][0] = mat[center + offset][0];
            mat[center+offset][0] = temp;
        }
    }
    else{
        center = width/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[0][i];
            mat[0][i] = mat[0][center + offset];
            mat[0][center+offset] = temp;
        }
    }
}


float **gaussian(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **gaussianDeriv(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        kernel[0][i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **transpose(float **mat, int width, int height){
    float **newMat;
    int i,j;
    if (width == height){
        newMat = create2D(width,height);
        for(i=0;i<height;i++){
            for(j=0;j<width;j++)newMat[i][j] = mat[j][i];
        }
    }
    else{
        newMat = create2D(height,width);
        if (width > height)for(j=0;j<width;j++)newMat[j][0] = mat[0][j];
        else for(j=0;j<height;j++)newMat[0][j] = mat[j][0];
    }
    return newMat;
}

float **create2D(int width, int height){
    int i;
    float **mat;
    mat = (float**)malloc(sizeof(float*)*height);
    for(i=0;i<height;i++)mat[i] = (float*)malloc(sizeof(float)*width);
    return mat;
}
template <class T>
void free2D(T **mat, int width, int height){
    int i;
    for(i=0;i<height;i++)free(mat[i]);
}

void printMat(float **mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i][j]);
        }
        printf("\n");
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

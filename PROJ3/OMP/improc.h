template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight,int chunkWidth, int chunkHeight, int kernelWidth, int kernelHeight, int offset);
template <class T, class U>
void magnitudePhase(T *vertical, T *horizontal, U *magnitude, U *gradient, int width, int height);
void rotate902D(float **mat, int width, int height);
void rotate1801D(float **mat, int width, int height);
float **gaussian(float sigma, int *size);
float **gaussianDeriv(float sigma, int *size);
float **transpose(float **mat, int width, int height);
float **create2D(int width, int height);
template <class T>
void free2D(T **mat, int width, int height);
void printMat(float **mat, int width, int height);
void printMat1D(float *mat, int width, int height);

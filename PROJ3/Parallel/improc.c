#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>
#include "improc.h"
#include "image_template.h"

int main(int argc,char *argv[]){
    float **kernel,**gkernel,**vkernel,**vgkernel,**rawPtr;
    float *raw;
    char fileNameHImage[100] = "horizontal_gradient.pgm";
    char fileNameVImage[100] = "vertical_gradient.pgm";
    char fileNameMImage[100] = "magnitude.pgm";
    char fileNamePImage[100] = "phase.pgm";
    rawPtr = &raw;

    int imWidth,imHeight,kernelSize,gkernelSize;
    int numThreads = 2;
    float sigma = .6;
    char *fn;

    if (argc < 4){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value> <Number_Threads>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
        numThreads = atoi(argv[3]);
    }

    read_image_template<float>(fn,rawPtr,&imWidth,&imHeight);
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *magnitude = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *phase = (float*)malloc(sizeof(float)*imWidth*imHeight);

    kernel = gaussian(sigma,&kernelSize);
    gkernel = gaussianDeriv(sigma,&gkernelSize);
    gkernel = rotate1801D(gkernel,gkernelSize,1);

    vkernel = transpose(kernel,kernelSize,1);
    vgkernel = transpose(gkernel,gkernelSize,1);

    //Initializing thread w/ joinable attribute
    pthread_t threads1[numThreads];
    pthread_t threads2[numThreads];
    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr);
    pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
    int t,rc;
    void *status;
    struct threadConvolveData tDataX[numThreads];
    struct threadConvolveData tDataY[numThreads];
    struct threadMagnitudePhaseData tDataMP[numThreads];
    int offset = (int)((imHeight*imWidth)/numThreads);
    int chunkWidth = imWidth;
    int chunkHeight = (int)(imHeight/numThreads);

    struct timeval start,end;
    gettimeofday(&start,NULL);

    for(t=0;t<numThreads;t++){

        // X - horizontal
        tDataX[t].threadId = t;
        tDataX[t].kernel = vkernel;
        tDataX[t].inputIm = raw + t*offset;
        tDataX[t].outputIm = partialSmoothImageX + t*offset;
        tDataX[t].width = imWidth;
        tDataX[t].height = imHeight;
        tDataX[t].chunkWidth = chunkWidth;
        tDataX[t].chunkHeight = chunkHeight;
        tDataX[t].kernelWidth = 1;
        tDataX[t].kernelHeight = kernelSize;
        tDataX[t].offset = t*offset;

        // Y - Vertical
        tDataY[t].threadId = t;
        tDataY[t].kernel = kernel;
        tDataY[t].inputIm = raw + t*offset;
        tDataY[t].outputIm = partialSmoothImageY + t*offset;
        tDataY[t].width = imWidth;
        tDataY[t].height = imHeight;
        tDataY[t].chunkWidth = chunkWidth;
        tDataY[t].chunkHeight = chunkHeight;
        tDataY[t].kernelWidth = kernelSize;
        tDataY[t].kernelHeight = 1;
        tDataY[t].offset = t*offset;

        rc = pthread_create(&threads1[t],&threadAttr,threadConvolve,(void *)&tDataX[t]);
        rc = pthread_create(&threads2[t],&threadAttr,threadConvolve,(void *)&tDataY[t]);
    }

    for(t=0;t<numThreads;t++){
        rc = pthread_join(threads1[t],&status);
        if (rc){
            printf("ERROR1: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
        rc = pthread_join(threads2[t],&status);
        if (rc){
            printf("ERROR2: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
    }

    for(t=0;t<numThreads;t++){

        // X - Horizontal
        tDataX[t].threadId = t;
        tDataX[t].kernel = gkernel;
        tDataX[t].inputIm = partialSmoothImageX + t*offset;
        tDataX[t].outputIm = Gx + t*offset;
        tDataX[t].width = imWidth;
        tDataX[t].height = imHeight;
        tDataX[t].chunkWidth = chunkWidth;
        tDataX[t].chunkHeight = chunkHeight;
        tDataX[t].kernelWidth = gkernelSize;
        tDataX[t].kernelHeight = 1;
        tDataX[t].offset = t*offset;

        // Y - Vertical
        tDataY[t].threadId = t;
        tDataY[t].kernel = vgkernel;
        tDataY[t].inputIm = partialSmoothImageY + t*offset;
        tDataY[t].outputIm = Gy + t*offset;
        tDataY[t].width = imWidth;
        tDataY[t].height = imHeight;
        tDataY[t].chunkWidth = chunkWidth;
        tDataY[t].chunkHeight = chunkHeight;
        tDataY[t].kernelWidth = 1;
        tDataY[t].kernelHeight = gkernelSize;
        tDataY[t].offset = t*offset;

        rc = pthread_create(&threads1[t],&threadAttr,threadConvolve,(void *)&tDataX[t]);
        rc = pthread_create(&threads2[t],&threadAttr,threadConvolve,(void *)&tDataY[t]);
    }

    pthread_attr_destroy(&threadAttr);
    for(t=0;t<numThreads;t++){
        rc = pthread_join(threads1[t],&status);
        if (rc){
            printf("ERROR1: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
        rc = pthread_join(threads2[t],&status);
        if (rc){
            printf("ERROR2: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
    }

    for(t=0;t<numThreads;t++){
        tDataMP[t].horizontal = Gx + t*offset;
        tDataMP[t].vertical = Gy + t*offset;
        tDataMP[t].magnitude = magnitude + t*offset;
        tDataMP[t].phase = phase + t*offset;
        tDataMP[t].width = imWidth;
        tDataMP[t].height = chunkHeight;
        rc = pthread_create(&threads1[t],&threadAttr,threadMagnitudePhase,(void *)&tDataMP[t]);
    }

    pthread_attr_destroy(&threadAttr);
    for(t=0;t<numThreads;t++){
        rc = pthread_join(threads1[t],&status);
        if (rc){
            printf("ERROR1: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
    }

    gettimeofday(&end,NULL);
    printf("Parallel Time: %lfs\n",((double)(end.tv_sec*1000000 + end.tv_usec)/1000000 - (double)(start.tv_sec*1000000 + start.tv_usec)/1000000));

    write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);
    write_image_template<float>(fileNameMImage,magnitude,imWidth,imHeight);
    write_image_template<float>(fileNamePImage,phase,imWidth,imHeight);

    free2D<float>(kernel,kernelSize,1);
    free2D<float>(gkernel,gkernelSize,1);
    free2D<float>(vkernel,1,kernelSize);
    free2D<float>(vgkernel,1,gkernelSize);
    free(raw);
    free(partialSmoothImageX);
    free(partialSmoothImageY);
    free(Gx);
    free(Gy);
    free(magnitude);
    free(phase);
    pthread_exit(NULL);

    return 0;
}

template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight,int chunkWidth, int chunkHeight, int kernelWidth, int kernelHeight, int offset){
    int rowCenter,colCenter,yBound,yCoord,xCoord;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);

    int i,j,y,x;
    float sop = 0;
    if(kernelWidth >= kernelHeight){
        for(j=0;j<chunkHeight;j++){
            for(i=0;i<chunkWidth;i++){
                sop = 0;
                for(y=0;y<kernelHeight;y++){
                    yBound = j - (rowCenter - y)  + (int)offset/(imWidth);
                    yCoord =  j - (rowCenter - y);
                    for(x=0;x<kernelWidth;x++){
                        //check boundaries
                        xCoord = i - (colCenter - x);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yBound < 0 || yBound > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
    else{
        for(j=0;j<chunkHeight;j++){
            for(i=0;i<chunkWidth;i++){
                sop = 0;
                for(x=0;x<kernelWidth;x++){
                    xCoord = i - (colCenter - x);
                    for(y=0;y<kernelHeight;y++){
                        //check boundaries
                        yBound = j - (rowCenter - y)  + (int)offset/(imWidth);
                        yCoord =  j - (rowCenter - y);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yBound < 0 || yBound > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
}

template <class T,class U>
void magnitudePhase(T *horizontal, T *vertical, U *magnitude, U *phase, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            magnitude[i*width + j] = sqrt(pow(vertical[i*width + j],2) + pow(horizontal[i*width + j],2));
            phase[i*width + j] = atan2(vertical[i*width + j],horizontal[i*width + j]);
        }
    }
}

float **rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float **newMat = create2D(width,height);
    center = height/2;
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                newMat[i][j] = mat[j][center + offset];
            }
        }
    }
    return newMat;
}

float **rotate1801D(float **mat, int width, int height){
    int center,i,offset;
    float **newMat = create2D(width,height);
    if(width < height){
        center = height/2;
        newMat[center][0] = mat[center][0];
        for(i=0;i<center;i++){
            offset = center - i;
            newMat[i][0] = mat[center + offset][0];
            newMat[center+offset][0] = mat[i][0];
        }
    }
    else{
        center = width/2;
        newMat[0][center] = mat[0][center];
        for(i=0;i<center;i++){
            offset = center - i;
            newMat[0][i] = mat[0][center + offset];
            newMat[0][center+offset] = mat[0][i];
        }
    }
    return newMat;
}


float **gaussian(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **gaussianDeriv(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        kernel[0][i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **transpose(float **mat, int width, int height){
    float **newMat;
    int i,j;
    if (width == height){
        newMat = create2D(width,height);
        for(i=0;i<height;i++){
            for(j=0;j<width;j++)newMat[i][j] = mat[j][i];
        }
    }
    else{
        newMat = create2D(height,width);
        if (width > height)for(j=0;j<width;j++)newMat[j][0] = mat[0][j];
        else for(j=0;j<height;j++)newMat[0][j] = mat[j][0];
    }
    return newMat;
}

float **create2D(int width, int height){
    int i;
    float **mat;
    mat = (float**)malloc(sizeof(float*)*height);
    for(i=0;i<height;i++)mat[i] = (float*)malloc(sizeof(float)*width);
    return mat;
}
template <class T>
void free2D(T **mat, int width, int height){
    int i;
    for(i=0;i<height;i++)free(mat[i]);
}

void printMat(float **mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i][j]);
        }
        printf("\n");
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

template <class T,class U>
void copy1D2D(T *mat1,U **mat2,int width,int height, int offset){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mat2[i][j] = (U)mat1[i*width + j + offset];
        }
    }
}

template <class T,class U>
void copy2D1D(T **mat1,U *mat2,int width,int height,int offset){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mat2[i*width + j + offset] = (U)mat1[i][j];
        }
    }
}

void *threadConvolve(void *arg){
    struct threadConvolveData *tData;
    tData = (struct threadConvolveData *)arg;
    float **kernel = tData->kernel;
    float *inputIm = tData->inputIm;
    float *outputIm = tData->outputIm;
    int width = tData->width;
    int height = tData->height;
    int chunkWidth = tData->chunkWidth;
    int chunkHeight = tData->chunkHeight;
    int kernelWidth = tData->kernelWidth;
    int kernelHeight = tData->kernelHeight;
    int offset = tData->offset;

    convolve(kernel,inputIm,outputIm,width,height,chunkWidth,chunkHeight,kernelWidth,kernelHeight,offset);

    pthread_exit(NULL);
}

void *threadMagnitudePhase(void *arg){
    struct threadMagnitudePhaseData *tData;
    tData = (struct threadMagnitudePhaseData *)arg;
    float *horizontal = tData->horizontal;
    float *vertical = tData->vertical;
    float *magnitude = tData->magnitude;
    float *phase = tData->phase;
    int width = tData->width;
    int height = tData->height;

    magnitudePhase<float,float>(horizontal,vertical,magnitude,phase,width,height);
    pthread_exit(NULL);
}


#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"

int main(){
    int image,imWidth,imHeight;
    char fileName[100];
    int *imagePtr1;
    int **imagePtr2;
    int *imWidthPtr;
    int *imHeightPtr;
    imagePtr1 = &image;
    imagePtr2 = &imagePtr1;
    imWidthPtr = &imWidth;
    imHeightPtr = &imHeight;
    printf("Please input complete file name:");
    scanf("%s",fileName);
    printf("%s\n",fileName);
    int p = 1;
    while(p%2 == 1){
        printf("Please input number of processor:");
        scanf("%d",&p);
    }
    read_image_template(fileName,imagePtr2,imWidthPtr,imHeightPtr);
    int chunkNum;
    int offset = 0;
    int chunkWidth = imWidth + 2;
    int chunkSize = 0;
    int chunkHeight = imHeight/p + 2;
    struct timeval start,end;
    int j = 0;
    gettimeofday(&start,NULL);

    for(chunkNum = 0; chunkNum < p;chunkNum++){
        //printf("%d-%d\n",chunkWidth,chunkHeight);
        int i;
        char outFile[20];
        if(imHeight%2 == 1 && chunkNum == p - 1)chunkHeight = imHeight/p + 3;
        chunkSize = chunkWidth*chunkHeight;
        printf("chunkSize : %d\n",chunkSize);
        int *tempImg=(int*)malloc(sizeof(int)*(chunkSize));
        sprintf(outFile,"op_%d_%d.pgm",chunkNum,p);
        offset = imWidth*(imHeight/p)*chunkNum;
        j = offset;
        for(i=chunkWidth; i < chunkSize-chunkWidth;i++){
            if(i%chunkWidth == 0 || i%chunkWidth == chunkWidth-1)continue;
            tempImg[i] = imagePtr1[j];
            j++;
        }
        //pad top
        if(chunkNum==0){
            for(i=1;i < chunkWidth-1;i++ ){
                tempImg[i] = tempImg[i+chunkWidth];
            }
        }
        else{
            for(i=1;i < chunkWidth-1;i++ ){
                tempImg[i] = imagePtr1[i+offset-1-imWidth];
            }
        }
        //pad bot
        if(chunkNum == p-1){
            for(i=chunkSize-chunkWidth+1;i < chunkSize - 1;i++ ){
                tempImg[i] = tempImg[i-chunkWidth];
            }
        }
        else{
            for(i=chunkSize-chunkWidth+1;i < chunkSize - 1;i++ ){
                tempImg[i] = imagePtr1[j];
                j++;
            }
        }
        for(i=0;i<chunkHeight;i++){
            tempImg[i*chunkWidth] = tempImg[i*chunkWidth + 1];
            tempImg[i*chunkWidth + chunkWidth - 1] = tempImg[i*chunkWidth + chunkWidth - 2];
        }
        write_image_template(outFile,tempImg,chunkWidth,chunkHeight);
        free(tempImg);
    }
    gettimeofday(&end,NULL);
    printf("Serial Time: %lfus\n",((double)(end.tv_sec*1000000 + end.tv_usec) - (double)(start.tv_sec*1000000 + start.tv_usec)));
    return 0;
}

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include "improc.h"
#include "image_template.h"

int main(int argc,char *argv[]){
    float **kernel,**gkernel,**vkernel,**vgkernel,**rawPtr,**image;
    float *raw;
    char fileNameHImage[100] = "horizontal_gradient.pgm";
    char fileNameVImage[100] = "vertical_gradient.pgm";
    rawPtr = &raw;

    int imWidth,imHeight,kernelSize,gkernelSize;
    int temp = 1;
    int numThreads = 2;
    float sigma = .6;

    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
    }
    char fn[100] = "Lenna_org_256.pgm";

    read_image_template<float>(fn,rawPtr,&imWidth,&imHeight);
    float *tempIm = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*imWidth*imHeight);
    image = create2D(imWidth,imHeight);
    copy1D2D<float,float>(raw,image,imWidth,imHeight,0);
    //partialSmoothImageX = create2D(imWidth,imHeight);
    //partialSmoothImageY = create2D(imWidth,imHeight);
    kernel = gaussian(sigma,&kernelSize);
    gkernel = gaussianDeriv(sigma,&gkernelSize);
    vkernel = transpose(kernel,kernelSize,1);
    vgkernel = transpose(gkernel,gkernelSize,1);

    //Initializing thread w/ joinable attribute
    pthread_t threads[numThreads];
    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr);
    pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
    int t,rc;
    void *status;
    struct threadConvolveData tData[numThreads];
    int offset = (int)((imHeight*imWidth)/numThreads);
    int chunkWidth = imWidth;
    int chunkHeight = (int)imHeight/numThreads;

    for(t=0;t<numThreads;t++){

        tData[t].threadId = t;
        tData[t].kernel = vkernel;
        tData[t].inputIm = raw;
        tData[t].outputIm = partialSmoothImageX;
        tData[t].chunkWidth= chunkWidth;
        tData[t].chunkWidth= chunkHeight;
        tData[t].width = imWidth;
        tData[t].height = imHeight;
        tData[t].kernelWidth = 1;
        tData[t].kernelHeight = kernelSize;
        tData[t].offset = t*offset;

        rc = pthread_create(&threads[t],&threadAttr,threadConvolve,(void *)&tData[t]);
    }

    pthread_attr_destroy(&threadAttr);
    for(t=0;t<numThreads;t++){
        rc = pthread_join(threads[t],&status);
        if (rc){
            printf("ERROR: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
    }

    for(t=0;t<numThreads;t++){

        tData[t].threadId = t;
        tData[t].kernel = gkernel;
        tData[t].inputIm = partialSmoothImageX;
        tData[t].outputIm = Gx;
        tData[t].width = imWidth;
        tData[t].height = imHeight/numThreads;
        tData[t].kernelWidth = gkernelSize;
        tData[t].kernelHeight = 1;
        tData[t].offset = t*offset;

        rc = pthread_create(&threads[t],&threadAttr,threadConvolve,(void *)&tData[t]);
    }

    pthread_attr_destroy(&threadAttr);
    for(t=0;t<numThreads;t++){
        rc = pthread_join(threads[t],&status);
        if (rc){
            printf("ERROR: Return code from pthread_join() is %d\n",rc);
            exit(-1);
        }
    }
    /*
    convolve(vkernel,image,partialSmoothImageX,imWidth,imHeight,1,kernelSize);
    convolve(gkernel,partialSmoothImageX,Gx,imWidth,imHeight,kernelSize,1);
    convolve(kernel,image,partialSmoothImageY,imWidth,imHeight,kernelSize,1);
    convolve(vgkernel,partialSmoothImageY,Gy,imWidth,imHeight,1,kernelSize);

    copy2D1D<float,float>(Gx,tempIm,imWidth,imHeight);
    write_image_template<float>(fileNameHImage,tempIm,imWidth,imHeight);
    copy2D1D<float,float>(Gy,tempIm,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,tempIm,imWidth,imHeight);
    */
    write_image_template<float>(fileNameHImage,partialSmoothImageX,imWidth,imHeight);

    free2D<float>(image,imWidth,imHeight);
    free2D<float>(kernel,kernelSize,1);
    free2D<float>(gkernel,gkernelSize,1);
    free2D<float>(vkernel,1,kernelSize);
    free2D<float>(vgkernel,1,gkernelSize);
    free(raw);
    free(partialSmoothImageX);
    free(partialSmoothImageY);
    free(Gx);
    free(Gy);
    pthread_exit(NULL);

    return 0;
}

template <class T>
void convolve(T **kernel, float **inputIm, float **outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight){
    int rowCenter,colCenter;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);
    if(kernelWidth == kernelHeight)rotate902D(kernel,kernelWidth,kernelHeight);
    else rotate1801D(kernel,kernelWidth,kernelHeight);

    int i,j,y,x;
    float sop = 0;
    //printf("%d %d\n",rowCenter,colCenter);
    if(kernelWidth >= kernelHeight){
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(y=0;y<kernelHeight;y++){
                    for(x=0;x<kernelWidth;x++){
                        //check boundaries
                        if(i - (colCenter - x) < 0 || i - (colCenter - x) > imWidth - 1 ||\
                          j - (rowCenter - y) < 0 || j - (rowCenter - y) > imHeight - 1 ){
                            continue;
                          }
                        else{
                            //printf("%d-%d\n",j - (rowCenter - y),i - (colCenter - x));
                            sop = inputIm[j - (rowCenter - y)][i - (colCenter - x)]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j][i] = sop;
            }
        }
    }
    else{
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(x=0;x<kernelWidth;x++){
                    for(y=0;y<kernelHeight;y++){
                        //check boundaries
                        if(i - (colCenter - x) < 0 || i - (colCenter - x) > imWidth - 1 ||\
                          j - (rowCenter - y) < 0 || j - (rowCenter - y) > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[j - (rowCenter - y)][i - (colCenter - x)]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j][i] = sop;
            }
        }
    }
}

void rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float matCopy[height][width];
    center = height/2;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            matCopy[i][j] = mat[i][j];
        }
    }
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                mat[i][j] = matCopy[j][center + offset];
            }
        }
    }
}

void rotate1801D(float **mat, int width, int height){
    float temp;
    int center,i,offset;
    if(width < height){
        center = height/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[i][0];
            mat[i][0] = mat[center + offset][0];
            mat[center+offset][0] = temp;
        }
    }
    else{
        center = width/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[0][i];
            mat[0][i] = mat[0][center + offset];
            mat[0][center+offset] = temp;
        }
    }
}


float **gaussian(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
        //printf("%f\n",kernel[0][i]);
    }
    //printf("%f\n",sum);
    return kernel;
}

float **gaussianDeriv(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        kernel[0][i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **transpose(float **mat, int width, int height){
    float **newMat;
    int i,j;
    if (width == height){
        newMat = create2D(width,height);
        for(i=0;i<height;i++){
            for(j=0;j<width;j++)newMat[i][j] = mat[j][i];
        }
    }
    else{
        newMat = create2D(height,width);
        if (width > height)for(j=0;j<width;j++)newMat[j][0] = mat[0][j];
        else for(j=0;j<height;j++)newMat[0][j] = mat[j][0];
    }
    return newMat;
}

float **create2D(int width, int height){
    int i;
    float **mat;
    mat = (float**)malloc(sizeof(float*)*height);
    for(i=0;i<height;i++)mat[i] = (float*)malloc(sizeof(float)*width);
    return mat;
}
template <class T>
void free2D(T **mat, int width, int height){
    int i;
    for(i=0;i<height;i++)free(mat[i]);
}

void printMat(float **mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i][j]);
        }
        printf("\n");
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

template <class T,class U>
void copy1D2D(T *mat1,U **mat2,int width,int height, int offset){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mat2[i][j] = (U)mat1[i*width + j + offset];
        }
    }
}

template <class T,class U>
void copy2D1D(T **mat1,U *mat2,int width,int height,int offset){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mat2[i*width + j + offset] = (U)mat1[i][j];
        }
    }
}

void *threadConvolve(void *arg){
    struct threadConvolveData *tData;
    tData = (struct threadConvolveData *)arg;
    int threadId = tData->threadId;
    float **kernel = tData->kernel;
    float *inputIm = tData->inputIm;
    float *outputIm = tData->outputIm;
    int chunkWidth = tData->chunkWidth;
    int chunkHeight = tData->chunkHeight;
    int width = tData->width;
    int height = tData->height;
    int kernelWidth = tData->kernelWidth;
    int kernelHeight = tData->kernelHeight;
    int offset = tData->offset;
    float **tempImage = create2D(width,height);
    float **tempImageOut = create2D(width,height);

    //printf("%d\n",width);

    //printMat(kernel,kernelWidth,kernelHeight);
    //printMat(inputIm,width,height);
    //printf("%f\n",inputIm[0][0]);
    copy1D2D(inputIm,tempImage,width,height,offset);
    convolve(kernel,tempImage,tempImageOut,width,height,kernelWidth,kernelHeight);
    copy2D1D(tempImageOut,outputIm,width,height,offset);

    free2D<float>(tempImage,width,height);
    free2D<float>(tempImageOut,width,height);

    //printf("Thread %d \n",threadId);
    pthread_exit(NULL);
}

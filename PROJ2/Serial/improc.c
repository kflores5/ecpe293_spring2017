#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "improc.h"
#include "image_template.h"

int main(int argc,char *argv[]){
    float **kernel,**gkernel,**vkernel,**vgkernel,**rawPtr;
    float *raw;
    char fileNameHImage[100] = "horizontal_gradient.pgm";
    char fileNameVImage[100] = "vertical_gradient.pgm";
    rawPtr = &raw;

    int imWidth,imHeight,kernelSize,gkernelSize;
    char *fn;
    float sigma;

    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
    }

    read_image_template<float>(fn,rawPtr,&imWidth,&imHeight);
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*imWidth*imHeight);

    kernel = gaussian(sigma,&kernelSize);
    gkernel = gaussianDeriv(sigma,&gkernelSize);
    vkernel = transpose(kernel,kernelSize,1);
    vgkernel = transpose(gkernel,kernelSize,1);

    struct timeval start,end;
    gettimeofday(&start,NULL);
    convolve(vkernel,raw,partialSmoothImageX,imWidth,imHeight,1,kernelSize);
    convolve(gkernel,partialSmoothImageX,Gx,imWidth,imHeight,kernelSize,1);
    convolve(kernel,raw,partialSmoothImageY,imWidth,imHeight,kernelSize,1);
    convolve(vgkernel,partialSmoothImageY,Gy,imWidth,imHeight,1,kernelSize);
    gettimeofday(&end,NULL);
    printf("Serial Time: %lfs\n",((double)(end.tv_sec*1000000 + end.tv_usec)/1000000 - (double)(start.tv_sec*1000000 + start.tv_usec)/1000000));

    write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);

    free2D<float>(kernel,kernelSize,1);
    free2D<float>(gkernel,gkernelSize,1);
    free2D<float>(vkernel,kernelSize,1);
    free2D<float>(vgkernel,kernelSize,1);
    free(partialSmoothImageX);
    free(partialSmoothImageY);
    free(Gx);
    free(Gy);
    free(raw);

    return 0;
}

template <class T>
void convolve(T **kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight){
    int rowCenter,colCenter,yCoord,xCoord;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);
    if(kernelWidth == kernelHeight)rotate902D(kernel,kernelWidth,kernelHeight);
    else rotate1801D(kernel,kernelWidth,kernelHeight);

    int i,j,y,x;
    float sop = 0;

    if(kernelWidth >= kernelHeight){
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(y=0;y<kernelHeight;y++){
                    yCoord =  j - (rowCenter - y);
                    for(x=0;x<kernelWidth;x++){
                        //check boundaries
                        xCoord = i - (colCenter - x);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yCoord < 0 || yCoord > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
    else{
        for(j=0;j<imHeight;j++){
            for(i=0;i<imWidth;i++){
                sop = 0;
                for(x=0;x<kernelWidth;x++){
                    xCoord = i - (colCenter - x);
                    for(y=0;y<kernelHeight;y++){
                        //check boundaries
                        yCoord =  j - (rowCenter - y);
                        if( xCoord < 0 || xCoord > imWidth - 1 ||\
                           yCoord < 0 || yCoord > imHeight - 1 ){
                            continue;
                          }
                        else{
                            sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[y][x]\
                                             + sop;
                        }
                    }
                }
                outputIm[j*imWidth + i] = sop;
            }
        }
    }
}

void rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float matCopy[height][width];
    center = height/2;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            matCopy[i][j] = mat[i][j];
        }
    }
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                mat[i][j] = matCopy[j][center + offset];
            }
        }
    }
}

void rotate1801D(float **mat, int width, int height){
    float temp;
    int center,i,offset;
    if(width < height){
        center = height/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[i][0];
            mat[i][0] = mat[center + offset][0];
            mat[center+offset][0] = temp;
        }
    }
    else{
        center = width/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[0][i];
            mat[0][i] = mat[0][center + offset];
            mat[0][center+offset] = temp;
        }
    }
}


float **gaussian(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **gaussianDeriv(float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    float **kernel;
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    kernel = (float**)malloc(sizeof(float*));
    kernel[0] = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        kernel[0][i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*kernel[0][i];
    }
    for(i=0;i<kernelSize;i++){
        kernel[0][i] = kernel[0][i]/sum;
    }
    return kernel;
}

float **transpose(float **mat, int width, int height){
    float **newMat;
    int i,j;
    if (width == height){
        newMat = create2D(width,height);
        for(i=0;i<height;i++){
            for(j=0;j<width;j++)newMat[i][j] = mat[j][i];
        }
    }
    else{
        newMat = create2D(height,width);
        if (width > height)for(j=0;j<width;j++)newMat[j][0] = mat[0][j];
        else for(j=0;j<height;j++)newMat[0][j] = mat[j][0];
    }
    return newMat;
}

float **create2D(int width, int height){
    int i;
    float **mat;
    mat = (float**)malloc(sizeof(float*)*height);
    for(i=0;i<height;i++)mat[i] = (float*)malloc(sizeof(float)*width);
    return mat;
}
template <class T>
void free2D(T **mat, int width, int height){
    int i;
    for(i=0;i<height;i++)free(mat[i]);
}

void printMat(float **mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i][j]);
        }
        printf("\n");
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

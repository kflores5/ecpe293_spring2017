#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "improc.h"
#include "image_template.h"

#define PI 3.14159265

int main(int argc,char *argv[]){
    float *kernel,*gkernel,*raw;
    char fileNameHImage[100] = "horizontal_gradient.pgm";
    char fileNameVImage[100] = "vertical_gradient.pgm";
    char fileNameMImage[100] = "mag.pgm";
    char fileNamePImage[100] = "phase.pgm";
    char fileNameNImage[100] = "nonMaxImage.pgm";
    char fileNameEImage[100] = "edge.pgm";

    int imWidth,imHeight,kernelSize,gkernelSize;
    char *fn;
    float sigma;

    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
    }

    read_image_template<float>(fn,&raw,&imWidth,&imHeight);
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageX = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialSmoothImageY = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *mag = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *phase = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *nonMaxImg = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *edgeImg = (float*)malloc(sizeof(float)*imWidth*imHeight);

    gaussian(&kernel,sigma,&kernelSize);
    gaussianDeriv(&gkernel,sigma,&gkernelSize);
    printMat1D(kernel,kernelSize,1);
    printMat1D(gkernel,kernelSize,1);

    struct timeval start,end;
    gettimeofday(&start,NULL);
    convolve<float>(kernel,raw,partialSmoothImageX,imWidth,imHeight,1,kernelSize);
    convolve<float>(gkernel,partialSmoothImageX,Gx,imWidth,imHeight,kernelSize,1);
    convolve<float>(kernel,raw,partialSmoothImageY,imWidth,imHeight,kernelSize,1);
    convolve<float>(gkernel,partialSmoothImageY,Gy,imWidth,imHeight,1,kernelSize);
    magnitudePhase<float,float>(Gx,Gy,mag,phase,imWidth,imHeight);
    nonMaxsuppression<float>(mag,phase,nonMaxImg,imWidth,imHeight);
    hysteresis<float>(nonMaxImg,edgeImg,imWidth,imHeight);
    gettimeofday(&end,NULL);
    printf("Serial Time: %lfs\n",((double)(end.tv_sec*1000000 + end.tv_usec)/1000000 - (double)(start.tv_sec*1000000 + start.tv_usec)/1000000));

    write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);
    write_image_template<float>(fileNameMImage,mag,imWidth,imHeight);
    write_image_template<float>(fileNamePImage,phase,imWidth,imHeight);
    write_image_template<float>(fileNameNImage,nonMaxImg,imWidth,imHeight);
    write_image_template<float>(fileNameEImage,edgeImg,imWidth,imHeight);

    free(kernel);
    free(gkernel);
    free(partialSmoothImageX);
    free(partialSmoothImageY);
    free(Gx);
    free(Gy);
    free(mag);
    free(phase);
    free(raw);
    free(nonMaxImg);
    free(edgeImg);

    return 0;
}

template <class T>
void convolve(T *kernel, T *inputIm, T *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight){
    int rowCenter,colCenter,yCoord,xCoord;
    rowCenter = (int)floor(kernelHeight/2);
    colCenter = (int)floor(kernelWidth/2);

    int i,j,y,x;
    int *k;
    float sop = 0;
    if(kernelWidth < kernelHeight) k=&y;
    else k=&x;

    for(j=0;j<imHeight;j++){
        for(i=0;i<imWidth;i++){
            sop = 0;
            for(y=0;y<kernelHeight;y++){
                yCoord =  j - (rowCenter - y);
                for(x=0;x<kernelWidth;x++){
                    //check boundaries
                    xCoord = i - (colCenter - x);
                    if( xCoord < 0 || xCoord > imWidth - 1 ||\
                       yCoord < 0 || yCoord > imHeight - 1 ){
                        continue;
                      }
                    else{
                        sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[*k]\
                                         + sop;
                    }
                }
            }
            outputIm[j*imWidth + i] = sop;
        }
    }
}

template <class T,class U>
void magnitudePhase(T *horizontal, T *vertical, U *mag, U *phase, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            mag[i*width + j] = sqrt(pow(vertical[i*width + j],2) + pow(horizontal[i*width + j],2));
            phase[i*width + j] = atan2(horizontal[i*width + j],vertical[i*width + j]);
        }
    }
}
template <class T>
void nonMaxsuppression(T *mag, T *phase, T *outImg, int width, int height){
    int i,j,k;
    float angle,startAngle,endAngle;

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            angle = phase[i*width + j];
            angle = angle*180/PI;
            if (angle < 0){
                angle = angle + 180;
            }
            if (angle <= 22.5 || angle > 157.5){
                phase[i*width + j] = 0;
            }
            else{
                startAngle = 22.5;
                for(k=0;k<3;k++){
                    endAngle = startAngle + 45;
                    if (angle > startAngle && angle <= endAngle){
                        phase[i*width + j] = startAngle + 22.5;
                        break;
                    }
                    startAngle = endAngle;
                }
            }
        }
    }

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            outImg[i*width + j] = findPeak<T>(mag,phase,i,j,width,height);
        }
    }
}

template <class T>
float findPeak(T *mag, T *phase, int i, int j, int width, int height){
    float angle,peak;
    int xOffset, yOffset;

    angle = phase[i*width + j];
    if (angle == 0){
        xOffset = 0;
        yOffset = -1;
    }
    else if(angle == 45){
        xOffset = -1;
        yOffset = -1;
    }
    else if(angle == 90){
        xOffset = 1;
        yOffset = 0;
    }
    else if(angle == 135){
        xOffset = 1;
        yOffset = -1;
    }
    peak = mag[i*width + j];
    if( i+1 < height && i-1 >= 0 && j+1 < width && j-1 >= 0 ){
        if( ( mag[i*width + j] < mag[(i+yOffset)*width + j + xOffset] && \
            phase[i*width + j] == phase[(i+yOffset)*width + j + xOffset] ) || \
            ( mag[i*width + j] < mag[(i-yOffset)*width + j - xOffset] && \
            phase[i*width + j] == phase[(i-yOffset)*width + j - xOffset] ) ) peak = 0;
    }
    else{
        if( i-yOffset < 0 || i-yOffset >= height || j-xOffset < 0 || j-xOffset >= width ){
            if( mag[i*width + j] < mag[(i+yOffset)*width + j + xOffset] && \
                phase[i*width + j] == phase[(i+yOffset)*width + j + xOffset] ) peak = 0;
        }
        if( i+yOffset < 0 || i+yOffset >= height || j+xOffset < 0 || j+xOffset >= width ){
            if( mag[i*width + j] < mag[(i-yOffset)*width + j - xOffset] && \
                phase[i*width + j] == phase[(i-yOffset)*width + j - xOffset] ) peak = 0;
        }
    }
    return peak;
}

template <class T>
void hysteresis(T *nonMaxImg, T *outImg, int width, int height){
    int i,j,x,y,terminate;
    int center = 1;
    float tHi,tLo;
    float *hImage = (float*)malloc(sizeof(float)*width*height);
    float *sortedIntensity = (float*)malloc(sizeof(float)*width*height);
    //Copy non maximal suppression image
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            sortedIntensity[i*width + j] = nonMaxImg[i*width + j];
        }
    }
    qsort(sortedIntensity,width*height,sizeof(*sortedIntensity),comp);
    printf("%f\n",sortedIntensity[width*height -1]);
    tHi = sortedIntensity[int(width*height*.9)];
    tLo = tHi/5;
    printf("%f %f\n",tHi,tLo);

    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if (!(i==0||i==height-1||j==0||j==width-1)){
                if(nonMaxImg[i*width + j] >= tHi)hImage[i*width + j] = 255;
                else if(nonMaxImg[i*width + j] < tHi && nonMaxImg[i*width + j] > tLo)hImage[i*width + j] = 125;
            }
            else hImage[i*width + j] = 125;
            outImg[i*width + j] = hImage[i*width + j];
        }
    }
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if (hImage[i*width + j] == 125){
                for(y=0;y<2;y++){
                    terminate = 0;
                    for(x=0;x<2;x++){
                        if(!(y==center&&x==center)){
                            if( j-(center-x) < 0 || j-(center-x) >= width || \
                                i-(center-y) < 0 || i-(center-y) >= height ) continue;
                            else{
                                if (hImage[(i-y)*width + j - x] == 255){
                                    outImg[i*width+j] = 255;
                                    terminate = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (terminate == 0) outImg[i*width+j] = 0;
            }
        }
    }

    free(hImage);
    free(sortedIntensity);
}

int comp( const void *a, const void *b ){
    float *x = (float *)a;
    float *y = (float *)b;
    if( *x < *y ) return -1;
    else if( *x > *y ) return 1;
    return 0;

}

void rotate902D(float **mat, int width, int height){
    int center,i,j,offset;
    float matCopy[height][width];
    center = height/2;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            matCopy[i][j] = mat[i][j];
        }
    }
    if(width == height){
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                offset = center - i;
                mat[i][j] = matCopy[j][center + offset];
            }
        }
    }
}

void rotate1801D(float **mat, int width, int height){
    float temp;
    int center,i,offset;
    if(width < height){
        center = height/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[i][0];
            mat[i][0] = mat[center + offset][0];
            mat[center+offset][0] = temp;
        }
    }
    else{
        center = width/2;
        for(i=0;i<center;i++){
            offset = center - i;
            temp = mat[0][i];
            mat[0][i] = mat[0][center + offset];
            mat[0][center+offset] = temp;
        }
    }
}


void gaussian(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + (*kernel)[i];
    }
    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = (*kernel)[i]/sum;
    }
}

void gaussianDeriv(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*(*kernel)[i];
    }

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = ((*kernel)[i])/sum;
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

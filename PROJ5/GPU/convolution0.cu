#include <stdio.h>
#include <cuda.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "convolution0.h"
#include "image_template.h"

#define PI 3.14159265

int main(int argc,char *argv[]){
    // CPU: Initializing variables
    float *kernel,*gkernel,*raw;
    char *fn;
    float sigma;
    int imWidth,imHeight,kernelSize,gkernelSize;
    char fileNameHImage[100] = "horizontal_gradient0.pgm";
    char fileNameVImage[100] = "vertical_gradient0.pgm";

    // GPU: Initializing variables
    float *devKernel, *devGkernel, *devRaw;

    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
    }
    // CPU: Reading image
    read_image_template<float>(fn,&raw,&imWidth,&imHeight);

    // CPU: Calculating gaussian and gaussian derivative
    gaussian(&kernel,sigma,&kernelSize);
    gaussianDeriv(&gkernel,sigma,&gkernelSize);
    rotate1801D(gkernel,kernelSize);

    // CPU: Initializing output variables
    float *Gx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *Gy = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialGx = (float*)malloc(sizeof(float)*imWidth*imHeight);
    float *partialGy = (float*)malloc(sizeof(float)*imWidth*imHeight);

    // GPU: Initializing output variables
    float *devGx, *devGy, *devPartialGx, *devPartialGy;

    // CPU mallocs for GPU buffers
    cudaMalloc((void **)&devKernel,sizeof(float)*kernelSize);
    cudaMalloc((void **)&devGkernel,sizeof(float)*gkernelSize);
    cudaMalloc((void **)&devRaw,sizeof(float)*imWidth*imHeight);
    cudaMalloc((void **)&devGx,sizeof(float)*imWidth*imHeight);
    cudaMalloc((void **)&devGy,sizeof(float)*imWidth*imHeight);
    cudaMalloc((void **)&devPartialGx,sizeof(float)*imWidth*imHeight);
    cudaMalloc((void **)&devPartialGy,sizeof(float)*imWidth*imHeight);

    // Copy data from CPU to GPU
    cudaMemcpy(devRaw,raw,sizeof(float)*imWidth*imHeight,cudaMemcpyHostToDevice);
    cudaMemcpy(devKernel,kernel,sizeof(float)*kernelSize,cudaMemcpyHostToDevice);
    cudaMemcpy(devGkernel,gkernel,sizeof(float)*kernelSize,cudaMemcpyHostToDevice);

    int blockDimension = 16;
    dim3 dimGrid(ceil(imHeight/blockDimension),ceil(imWidth/blockDimension),1);
    dim3 dimBlock(blockDimension,blockDimension,1);

    float totalTime;
    cudaEvent_t start,stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    convolve<<<dimGrid,dimBlock>>>(devKernel,devRaw,devPartialGx,imWidth,imHeight,1,kernelSize);
    convolve<<<dimGrid,dimBlock>>>(devGkernel,devPartialGx,devGx,imWidth,imHeight,kernelSize,1);
    convolve<<<dimGrid,dimBlock>>>(devKernel,devRaw,devPartialGy,imWidth,imHeight,kernelSize,1);
    convolve<<<dimGrid,dimBlock>>>(devGkernel,devPartialGy,devGy,imWidth,imHeight,1,kernelSize);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&totalTime,start,stop);
    printf("Convolution Level 0 Time: %fms\n",totalTime);

    cudaMemcpy(Gx,devGx,sizeof(float)*imWidth*imHeight,cudaMemcpyDeviceToHost);
    cudaMemcpy(Gy,devGy,sizeof(float)*imWidth*imHeight,cudaMemcpyDeviceToHost);

    write_image_template<float>(fileNameHImage,Gx,imWidth,imHeight);
    write_image_template<float>(fileNameVImage,Gy,imWidth,imHeight);

    free(kernel);
    free(gkernel);
    free(partialGx);
    free(partialGy);
    free(Gx);
    free(Gy);
    free(raw);
    cudaFree(devKernel);
    cudaFree(devGkernel);
    cudaFree(devRaw);
    cudaFree(devGx);
    cudaFree(devGy);
    cudaFree(devPartialGx);
    cudaFree(devPartialGy);

    return 0;
}

__global__
void convolve(float *kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight){
    int rowCenter,colCenter,yCoord,xCoord;
    rowCenter = (int)kernelHeight/2;
    colCenter = (int)kernelWidth/2;

    int i,j,y,x;
    int *k;
    float sop = 0;
    if(kernelWidth < kernelHeight) k=&y;
    else k=&x;

    i = blockIdx.x*blockDim.x + threadIdx.x;
    j = blockIdx.y*blockDim.y + threadIdx.y;
    if(i < imWidth && j < imHeight){
        sop = 0;
        for(y=0;y<kernelHeight;y++){
            yCoord =  j - (rowCenter - y);
            for(x=0;x<kernelWidth;x++){
                //check boundaries
                xCoord = i - (colCenter - x);
                if( xCoord < 0 || xCoord > imWidth - 1 ||\
                   yCoord < 0 || yCoord > imHeight - 1 ){
                    continue;
                  }
                else{
                    sop = inputIm[yCoord*imWidth + xCoord]*(float)kernel[*k]\
                                     + sop;
                }
            }
        }
        outputIm[j*imWidth + i] = sop;
    }
}

void rotate1801D(float *mat, int width){
    float temp;
    int center,i,offset;
    center = width/2;
    for(i=0;i<center;i++){
        offset = center - i;
        temp = mat[i];
        mat[i] = mat[center + offset];
        mat[center+offset] = temp;
    }
}

void gaussian(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + (*kernel)[i];
    }
    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = (*kernel)[i]/sum;
    }
}

void gaussianDeriv(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*(*kernel)[i];
    }

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = ((*kernel)[i])/sum;
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

#include <stdio.h>
#include <cuda.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "convolution2.h"
#include "image_template.h"

#define PI 3.14159265
#define KSIZE 3
#define TILEWIDTH 16

__constant__ float devKernel[32];
__constant__ float devGkernel[32];

int main(int argc,char *argv[]){
    // CPU: Initializing variables
    float *kernel,*gkernel,*raw;
    char *fn;
    float sigma;
    int width,height,kernelSize,gkernelSize;
    char fileNameHImage[100] = "horizontal_gradient2.pgm";
    char fileNameVImage[100] = "vertical_gradient2.pgm";

    // GPU: Initializing variables
    float *devRaw;

    if (argc < 3){
        printf("Not enough Input: <Program_Name> <File_Name> <Sigma_Value>\nExiting Program.\n");
        exit(0);
    }
    else{
        fn = argv[1];
        sigma = atof(argv[2]);
    }
    // CPU: Reading image
    read_image_template<float>(fn,&raw,&width,&height);

    // CPU: Calculating gaussian and gaussian derivative
    gaussian(&kernel,sigma,&kernelSize);
    gaussianDeriv(&gkernel,sigma,&gkernelSize);
    rotate1801D(gkernel,kernelSize);

    // CPU: Initializing output variables
    float *Gx = (float*)malloc(sizeof(float)*width*height);
    float *Gy = (float*)malloc(sizeof(float)*width*height);
    float *partialGx = (float*)malloc(sizeof(float)*width*height);
    float *partialGy = (float*)malloc(sizeof(float)*width*height);

    // GPU: Initializing output variables
    float *devGx, *devGy, *devPartialGx, *devPartialGy;

    // CPU mallocs for GPU buffers
    cudaMalloc((void **)&devRaw,sizeof(float)*width*height);
    cudaMalloc((void **)&devGx,sizeof(float)*width*height);
    cudaMalloc((void **)&devGy,sizeof(float)*width*height);
    cudaMalloc((void **)&devPartialGx,sizeof(float)*width*height);
    cudaMalloc((void **)&devPartialGy,sizeof(float)*width*height);

    // Copy data from CPU to GPU
    cudaMemcpy(devRaw,raw,sizeof(float)*width*height,cudaMemcpyHostToDevice);
    // CPU: Copying kernel and gkernel to constant memory
    cudaMemcpyToSymbol(devKernel,kernel,sizeof(float)*kernelSize,0);
    cudaMemcpyToSymbol(devGkernel,gkernel,sizeof(float)*gkernelSize,0);

    int blockDimension = TILEWIDTH;
    dim3 dimGrid(ceil(width/blockDimension),ceil(height/blockDimension),1);
    dim3 dimBlock(blockDimension,blockDimension,1);

    float totalTime;
    cudaEvent_t start,stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    convolveGaussianCol<<<dimGrid,dimBlock>>>(devRaw,devPartialGx,width,height,kernelSize);
    convolveGaussianDerivRow<<<dimGrid,dimBlock>>>(devPartialGx,devGx,width,height,kernelSize);
    convolveGaussianRow<<<dimGrid,dimBlock>>>(devRaw,devPartialGy,width,height,kernelSize);
    convolveGaussianDerivCol<<<dimGrid,dimBlock>>>(devPartialGy,devGy,width,height,kernelSize);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&totalTime,start,stop);
    printf("Convolution Level 2 Time: %fms\n",totalTime);

    cudaMemcpy(Gx,devGx,sizeof(float)*width*height,cudaMemcpyDeviceToHost);
    cudaMemcpy(Gy,devGy,sizeof(float)*width*height,cudaMemcpyDeviceToHost);

    write_image_template<float>(fileNameHImage,Gx,width,height);
    write_image_template<float>(fileNameVImage,Gy,width,height);

    free(kernel);
    free(gkernel);
    free(partialGx);
    free(partialGy);
    free(Gx);
    free(Gy);
    free(raw);
    cudaFree(devRaw);
    cudaFree(devGx);
    cudaFree(devGy);
    cudaFree(devPartialGx);
    cudaFree(devPartialGy);

    return 0;
}

__global__
void convolveGaussianRow(float *inputIm, float *outputIm, int width, int height, int kernelWidth){
    int center,offset;
    center = kernelWidth/2;
    __shared__ float data[TILEWIDTH][TILEWIDTH];
    int i,j,x;
    float sop = 0;
    int tx = threadIdx.x;
    i = blockIdx.x*blockDim.x + threadIdx.x;
    j = blockIdx.y*blockDim.y + threadIdx.y;

    data[threadIdx.y][threadIdx.x] = inputIm[j*width + i];

    __syncthreads();

    sop = 0;
    for(x=0;x<kernelWidth;x++){
        offset = x - center;
        if(((tx + offset) >= 0) && ((tx + offset) < blockDim.x))
            sop += data[threadIdx.y][tx + offset]*devKernel[x];
        else
            sop += inputIm[j*width + i + offset]*devKernel[x];
    }
    outputIm[j*width + i] = sop;
}

__global__
void convolveGaussianCol(float *inputIm, float *outputIm, int width, int height, int kernelWidth){
    int center,offset;
    __shared__ float data[TILEWIDTH][TILEWIDTH];
    center = kernelWidth/2;
    int i,j,y;
    float sop = 0;
    int ty = threadIdx.y;

    i = blockIdx.x*blockDim.x + threadIdx.x;
    j = blockIdx.y*blockDim.y + threadIdx.y;

    data[threadIdx.y][threadIdx.x] = inputIm[j*width + i];

    __syncthreads();

    sop = 0;
    for(y=0;y<kernelWidth;y++){
        offset = y - center;
        if(((ty + offset) >= 0) && ((ty + offset) < blockDim.y))
            sop += data[ty + offset][threadIdx.x]*devKernel[y];
        else
            sop += inputIm[(j+offset)*width + i]*devKernel[y];
    }
    outputIm[j*width + i] = sop;
}

__global__
void convolveGaussianDerivRow(float *inputIm, float *outputIm, int width, int height, int kernelWidth){
    int center,offset;
    center = kernelWidth/2;
    __shared__ float data[TILEWIDTH][TILEWIDTH];
    int i,j,x;
    float sop = 0;
    int tx = threadIdx.x;

    i = blockIdx.x*blockDim.x + threadIdx.x;
    j = blockIdx.y*blockDim.y + threadIdx.y;

    data[threadIdx.y][threadIdx.x] = inputIm[j*width + i];

    __syncthreads();

    sop = 0;
    for(x=0;x<kernelWidth;x++){
        offset = x - center;
        if(((tx + offset) >= 0) && ((tx + offset) < blockDim.x))
            sop += data[threadIdx.y][tx + offset]*devGkernel[x];
        else
            sop += inputIm[j*width + i + offset]*devGkernel[x];
    }
    outputIm[j*width + i] = sop;
}

__global__
void convolveGaussianDerivCol(float *inputIm, float *outputIm, int width, int height, int kernelWidth){
    int center,offset;
    __shared__ float data[TILEWIDTH][TILEWIDTH];
    center = kernelWidth/2;
    int i,j,y;
    float sop = 0;
    int ty = threadIdx.y;

    i = blockIdx.x*blockDim.x + threadIdx.x;
    j = blockIdx.y*blockDim.y + threadIdx.y;

    data[threadIdx.y][threadIdx.x] = inputIm[j*width + i];

    __syncthreads();

    sop = 0;
    for(y=0;y<kernelWidth;y++){
        offset = y - center;
        if(((j + offset) < 0) || ((j + offset) > height - 1 )){
            continue;
        }
        else{
            if(((ty + offset) >= 0) && ((ty + offset) < blockDim.y))
                sop += data[ty + offset][threadIdx.x]*devGkernel[y];
            else
                sop += inputIm[(j+offset)*width + i]*devGkernel[y];
        }
    }
    outputIm[j*width + i] = sop;
}

void rotate1801D(float *mat, int width){
    float temp;
    int center,i,offset;
    center = width/2;
    for(i=0;i<center;i++){
        offset = center - i;
        temp = mat[i];
        mat[i] = mat[center + offset];
        mat[center+offset] = temp;
    }
}

void gaussian(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum + (*kernel)[i];
    }
    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = (*kernel)[i]/sum;
    }
}

void gaussianDeriv(float **kernel, float sigma, int *size){
    float a = round(2.5*sigma-0.5);
    int kernelSize = (int)(2*a)+1;
    int i;
    float sum = 0;
    *size = kernelSize;

    *kernel = (float*)malloc(sizeof(float)*kernelSize);

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = -1.0*(float)(i-a)*exp(-pow(i-a,2)/(2*pow(sigma,2)));
        sum = sum - i*(*kernel)[i];
    }

    for(i=0;i<kernelSize;i++){
        (*kernel)[i] = ((*kernel)[i])/sum;
    }
}

void printMat1D(float *mat, int width, int height){
    int i,j;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            printf("%f ",mat[i*width+j]);
        }
        printf("\n");
    }
}

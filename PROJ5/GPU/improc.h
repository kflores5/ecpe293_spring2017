__global__
void convolve(float *kernel, float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth, int kernelHeight);
void rotate1801D(float *mat, int width);
void gaussian(float **kernel, float sigma, int *size);
void gaussianDeriv(float **gkernel, float sigma, int *size);
void printMat1D(float *mat, int width, int height);

__global__
void convolveGaussianRow(float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth);
__global__
void convolveGaussianCol(float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth);
__global__
void convolveGaussianDerivRow(float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth);
__global__
void convolveGaussianDerivCol(float *inputIm, float *outputIm, int imWidth, int imHeight, int kernelWidth);
void rotate1801D(float *mat, int width);
void gaussian(float **kernel, float sigma, int *size);
void gaussianDeriv(float **gkernel, float sigma, int *size);
void printMat1D(float *mat, int width, int height);
